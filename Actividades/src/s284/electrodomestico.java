/**
 * 
 */
package s284;

/**
 * @author Oriol
 *
 */
public class electrodomestico {
private String nombre;
private String color;

	public electrodomestico(){
		this.nombre="PC";
		this.color="RED";
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
