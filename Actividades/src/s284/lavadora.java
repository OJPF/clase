/**
 * 
 */
package s284;

/**
 * @author Oriol
 *
 */
public class lavadora  extends electrodomestico{

	private int potencia;
	
	public lavadora(){
		super();
		this.potencia=350;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}
	
}
