/**
 * 
 */
package Personas;

/**
 * @author Oriol
 *
 */
public class Persona {

	//Atributos
	private String nombre;
	private int edad;
	private String DNI;
	private String sexo;
	private int peso;
	private int altura;
	//Constructores
	public Persona(){
		this.nombre="";
		this.edad=0;
		this.DNI="39911036X";
		this.sexo="H";
		this.peso=0;
		this.altura=0;
	}
	public Persona(String nombre,int edad, String sexo){
		this.nombre="Oriol";
		this.edad=23;
		this.DNI="39911036X";
		this.sexo="H";
		this.peso=0;
		this.altura=0;
	}
	public Persona(String nombre,int edad, String DNI, String sexo, int peso, int altura){
		this.nombre="Oriol";
		this.edad=23;
		this.DNI="39911036X";
		this.sexo="H";
		this.peso=12;
		this.altura=35;
	}
}
