/**
 * 
 */
package Empleados;

/**
 * @author Oriol
 *
 */
public class Empleados {
	//Atributos
	private String nombre;
	private String apellido;
	private int edad;
	private double salario;
	
	//Metodos
	public boolean plus (double sueldoPlus){
		boolean aumento=false;
			if (edad>40){
				salario+=sueldoPlus;
				aumento=true;
			}
			return aumento;
			}
	//Constructores
	public Empleados(){
		this.nombre="";
		this.apellido="";
		this.edad=0;
		this.salario=0;
	}
	public Empleados(String nombre, String apellido, int edad, double salario){
		this.nombre=nombre;
		this.apellido=apellido;
		this.edad=edad;
		this.salario=salario;
	}

	}
