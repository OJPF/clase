package Swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EX2 extends JFrame {

	private JPanel contentPane;
	private JTextField txtPeli;
	/**
	 * Create the frame.
	 */
	public EX2() {
		setTitle("Peliculas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane txtpnEscribeUnaPeli = new JTextPane();
		txtpnEscribeUnaPeli.setText("Escribe una peli");
		txtpnEscribeUnaPeli.setBounds(52, 49, 95, 20);
		contentPane.add(txtpnEscribeUnaPeli);
		
		txtPeli = new JTextField();
		txtPeli.setText("Peli");
		txtPeli.setBounds(52, 80, 86, 20);
		contentPane.add(txtPeli);
		txtPeli.setColumns(10);
		
		JTextPane txtpnPeliculas = new JTextPane();
		txtpnPeliculas.setText("Peliculas");
		txtpnPeliculas.setBounds(252, 49, 52, 20);
		contentPane.add(txtpnPeliculas);
		
		JComboBox film = new JComboBox();
		film.setBounds(252, 92, 28, 20);
		contentPane.add(film);
		
		JButton btnNewButton = new JButton("A\u00F1adir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setHorizontalAlignment(SwingConstants.RIGHT);
		btnNewButton.setBounds(52, 118, 89, 23);
		contentPane.add(btnNewButton);}
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            actionPerformed(evt);
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);}
}
