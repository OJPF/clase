package Swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class EX1 extends JFrame {

	private JPanel contentPane;
	private JTextField txtEscribeUnNombre;


	/**
	 * Create the frame.
	 */
	public EX1() {
		setTitle("Saludador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtEscribeUnNombre = new JTextField();
		txtEscribeUnNombre.setText("Escribe un nombre para saludar:");
		txtEscribeUnNombre.setBounds(124, 89, 162, 20);
		contentPane.add(txtEscribeUnNombre);
		txtEscribeUnNombre.setColumns(10);
		
		JButton btnNewButton = new JButton("Saludar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,txtEscribeUnNombre.getText());
			}
		});
		btnNewButton.setBounds(163, 120, 89, 23);
		contentPane.add(btnNewButton);
	}
}
