package Swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JFormattedTextField;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;

public class EX3 extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EX3 frame = new EX3();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EX3() {
		setTitle("Encuesta");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Windows", true);
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(6, 37, 109, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Linux", false);
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(6, 63, 109, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("MAC", false);
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.setBounds(6, 89, 109, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JFormattedTextField frmtdtxtfldSo = new JFormattedTextField();
		frmtdtxtfldSo.setText("S.O");
		frmtdtxtfldSo.setBounds(10, 11, 40, 20);
		contentPane.add(frmtdtxtfldSo);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Programaci\u00F3n");
		chckbxNewCheckBox.setBounds(200, 37, 97, 23);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Dise\u00F1o");
		chckbxNewCheckBox_1.setBounds(200, 63, 97, 23);
		contentPane.add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxAdministracin = new JCheckBox("Administraci\u00F3n");
		chckbxAdministracin.setBounds(200, 89, 97, 23);
		contentPane.add(chckbxAdministracin);
		
		JFormattedTextField frmtdtxtfldEspecialidad = new JFormattedTextField();
		frmtdtxtfldEspecialidad.setText("Especialidad");
		frmtdtxtfldEspecialidad.setBounds(200, 11, 71, 20);
		contentPane.add(frmtdtxtfldEspecialidad);
		
		JSlider slider = new JSlider();
		slider.setPaintTrack(false);
		slider.setPaintTicks(true);
		slider.setMaximum(10);
		slider.setMinimum(1);
		slider.setBounds(6, 163, 200, 26);
		contentPane.add(slider);
		
		JTextPane txtpnHorasQueDedicas = new JTextPane();
		txtpnHorasQueDedicas.setText("Horas que dedicas al Ordenador");
		txtpnHorasQueDedicas.setBounds(10, 132, 217, 20);
		contentPane.add(txtpnHorasQueDedicas);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setBounds(313, 166, 89, 23);
		contentPane.add(btnEnviar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(225, 163, 46, 14);
		contentPane.add(lblNewLabel);
	}
}
