/**
 * 
 */
package Swing;
import javax.swing.*;
import java.awt.*;
/**
 * @author Oriol
 *
 */

public class AplicacionGrafica extends JFrame{
	private JPanel contentPane;
	/**
	 * 
	 */
	public AplicacionGrafica(){
		setTitle("Ventana");
		setBounds(400,200,250,300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		contentPane =new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		// Creacion de componentes
		JLabel etiqueta=new JLabel("�Hola Mundo!");
		etiqueta.setBounds(60,20,100,20);
		contentPane.add(etiqueta);
		
		// Campo de texto
		JTextField textField = new JTextField();
		textField.setBounds(43,67,86,26);
		contentPane.add(textField);
		
		//Bot�n
		JButton btnPulsame = new JButton("Pulsame");
		btnPulsame.setBounds(43,133,89,23);;
		contentPane.add(btnPulsame);
		
		// Bot�n radio
		JRadioButton rdbtnOpcion= new JRadioButton("Opcion 1", true);
		rdbtnOpcion.setBounds(43,194,109,23);
		contentPane.add(rdbtnOpcion);
		JRadioButton rdbtnOpcion1= new JRadioButton("Opcion 1", true);
		rdbtnOpcion.setBounds(43,220,109,23);
		contentPane.add(rdbtnOpcion);
		JRadioButton rdbtnOpcion2= new JRadioButton("Opcion 1", true);
		rdbtnOpcion.setBounds(43,246,109,23);
		contentPane.add(rdbtnOpcion);
		
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(rdbtnOpcion);
		bgroup.add(rdbtnOpcion1);
		bgroup.add(rdbtnOpcion2);
		
		//Boton checkbox
		JCheckBox chckbxOpcion = new JCheckBox("Opcion1",true);
		chckbxOpcion.setBounds(43,305,97,23);
		contentPane.add(chckbxOpcion);
		
		JCheckBox chckbxNewOpcion = new JCheckBox("Opcion2",true);
		chckbxOpcion.setBounds(43,325,97,23);
		contentPane.add(chckbxNewOpcion);
		
		JCheckBox chckbxOpcion1 = new JCheckBox("Opcion3",true);
		chckbxOpcion.setBounds(43,346,97,23);
		contentPane.add(chckbxOpcion1);
		
		//Texto de �rea
		JTextArea textArea = new JTextArea("prueba");
		textArea.setBounds(189,18,141,117);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		contentPane.add(textArea);
		
	    //Scroll
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(189,18,141,117);;
		contentPane.add(scroll);
	
		//campo password
		JPasswordField pwd = new JPasswordField("frenando");
		pwd.setBounds(189, 171, 139, 20);
		contentPane.add(pwd);
		
		//Men� de selecci�n
		JComboBox comboBox = new JComboBox<>();
		comboBox.setBounds(189, 221, 141, 22);
		contentPane.add(comboBox);
		
		comboBox.addItem("Fernando");
		comboBox.addItem("Alberto");
		comboBox.addItem("Arturo");
		
		// Interruptor
		JToggleButton tglbtnNewToggleButton = new JToggleButton("Interruptor",true);
		tglbtnNewToggleButton.setBounds(189, 291, 121, 23);
		contentPane.add(tglbtnNewToggleButton);
		
		// JSpinner
		JSpinner spinner = new JSpinner();
		spinner.setBounds(371, 20, 29, 20);
		contentPane.add(spinner);
		
		//Lista
		String pelis[]={"Star Wars", "Revolution", "007"};
		JList list = new JList<>(pelis);
		list.setBounds(371, 72, 86, 80);
		contentPane.add(list);
		
		//Menu
		JMenuBar barraMenu = new JMenuBar();
		JMenu archivo = new JMenu("Archivo");
		barraMenu.add(archivo);
		JMenu editar = new JMenu("Editar");
		barraMenu.add(editar);
		
		JMenuItem abrir= new JMenuItem("Abrir");
		JMenuItem guardar= new JMenuItem("Guardar");
		JMenuItem cargar= new JMenuItem("Cargar");
		JMenuItem salir= new JMenuItem("Salir");

		JMenuItem modificar= new JMenuItem("Modificar");
		JMenuItem copiar= new JMenuItem("Copiar");
		JMenuItem pegar= new JMenuItem("Pegar");
		
		archivo.add(abrir);
		archivo.add(guardar);
		archivo.add(cargar);
		archivo.add(salir);
		
		archivo.add(modificar);
		archivo.add(copiar);
		
		setJMenuBar(barraMenu);
	}
}

