/**
 * 
 */
package Operadores;

/**
 * @author Oriol
 *
 */
public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int operador=10;
		int x=0;
		operador++; //operador = 11
		operador++; //operador = 12
		operador--; //operador = 11
		
		x=operador++; //x = 11, operador = 12
		x=++operador; //x = 13, operador = 13
	}

}
