/**
 * 
 */
package Operadores;

/**
 * @author Oriol
 *
 */
public class E5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 30;
		int num2 = 20;
		int resultado=0;
		
		resultado= num1+num2; //resultado = 35
		System.out.println(resultado);
		resultado= num1-num2; //resultado =5
		System.out.println(resultado);
		resultado= num1*num2; //resultado = 30
		System.out.println(resultado);
		resultado= num1/num2; //resultado =1
		System.out.println(resultado);
		resultado= num1%num2; //resultado =5
		System.out.println(resultado);
	}

}
