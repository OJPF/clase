/**
 * 
 */
package Usodedatos;

/**
 * @author Oriol
 *
 */
import javax.swing.JOptionPane;
public class E6 {

	/**
	 * @param args
	 */
    public static void main(String[] args) {
        final double IVA=0.21;
 
        String texto=JOptionPane.showInputDialog("Precio del producto");
        double precio=Double.parseDouble(texto);
        double precioFinal=precio+(precio*IVA);
        System.out.println(precioFinal);
        JOptionPane.showMessageDialog(null,"El precio final del producto es: "+precioFinal);
    }
}
