/**
 * 
 */
package Usodedatos;

/**
 * @author Oriol
 *
 */
public class Eje6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double operador1=25.5;
		double operador2=15.21;
		
		System.out.println(Math.ceil(operador1));
		System.out.println(Math.floor(operador2));
		System.out.println(Math.pow(operador1, operador2));
		System.out.println(Math.max(operador1, operador2));
		System.out.println(Math.sqrt(operador1));
		
	}

}
