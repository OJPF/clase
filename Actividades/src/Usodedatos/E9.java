/**
 * 
 */
package Usodedatos;

/**
 * @author Oriol
 *
 */
public class E9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 
        int num=1;
        while (num<=100){
            if (num%2==0 || num%3==0){
                System.out.println(num);
            }
            num++;
        }
    }
}