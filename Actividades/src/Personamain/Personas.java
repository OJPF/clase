/**
 * 
 */
package Personamain;

import Personas.Persona;

/**
 * @author Oriol
 *
 */
public class Personas {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Persona persona1=new Persona ("Adri�",24,"39911035X","H",50,180);
		//Devolvera el uso del 3 constructor
		Persona persona2=new Persona ();
		//Devolvera el uso del 1 constructor
		Persona persona3=new Persona ("Oriol",24,"H");
		//Devolvera el uso del 2 constructor
		System.out.println(persona1);
		System.out.println(persona2);
	}

}
