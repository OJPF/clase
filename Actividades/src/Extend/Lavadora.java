package Extend;
/**
 * @author Oriol
 *
 */
public class Lavadora extends Electrodomestico {

private int carga;

	public Lavadora(){
	super();
	this.carga=5;
	}
	
    public Lavadora(int preciobase,String color, String consumoenergetico, int peso, int carga){
        super(preciobase,color,consumoenergetico,peso);
        this.carga=carga;
    }
    
    // Metodo precio final lavadora.
    
	public double precioFinal(){
        double precioplus=super.precioFinal();
        if (carga>30){
            precioplus+=50;
        }
 
        return precioplus;
    }
	public void setCarga(int carga) {
		this.carga = carga;
	}
	public int getCarga() {
		return carga;
	}

}
