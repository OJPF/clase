/**
 * 
 */
package Extend;

/**
 * @author Oriol
 *
 */

public class Television extends Electrodomestico {


	private int resolucion;
	private boolean sintonizadorTDT;
	
	// Constructor que llama a Electrodomesticos
	public Television(int preciobase,String color, String consumoenergetico, int peso, int resolucion, boolean sintonizadorTDT){
		super(preciobase, color, consumoenergetico, peso);
		this.resolucion=resolucion;
		this.sintonizadorTDT=sintonizadorTDT;
		}
	// Constructor que llama a los valores por defecto
	public Television(){
		this.resolucion=20;
		this.sintonizadorTDT=false;
		}
	// Constructor que llama a los valores por defecto + valores llamados
    public Television(int preciobase, int peso){
    	super (preciobase,peso);
		this.resolucion=20;
		this.sintonizadorTDT=false;
    }
		//Precio final
		public double precioFinal(){
		 double precioplus=super.precioFinal();
        if (resolucion>40){
        	precioplus+=getPreciobase()*0.3;
        }
        if (sintonizadorTDT=true){
        	precioplus+=50;
        }
 
        return precioplus;
    } 
}
