/**
 * 
 */
package Extend;

/**
 * @author Oriol
 *
 */
public class Electrodomestico {

	private int preciobase;
	private String color;
	private String consumoenergetico;
	private int peso;
	private int precioFinal;
	private String error="Error datos erroneos";

		public Electrodomestico(){
			this.preciobase=350;
			this.color="blanco";
			this.consumoenergetico="F";
			this.peso=5;
			
	}
		public Electrodomestico(int preciobase,int peso){
			this.preciobase=preciobase;
			this.color="blanco";
			this.consumoenergetico="F";
			this.peso=peso;
			
	}
		public Electrodomestico(int preciobase,String color, String consumoenergetico, int peso){
			this.preciobase=preciobase;
			this.color="blanco";
			this.consumoenergetico="F";
			this.peso=peso;
			
	}
	    private void consumoenergetico(String conener){
	    	 
	        String conenerg[]={"A", "B", "C", "D", "E", "F"};
	        boolean ener=false;
	 
	        for(int i=0;i<conenerg.length && !ener;i++){
	 
	            if(conenerg[i].equals(color)){
	                ener=true;
	            }
	 
	        }
	 
	        if(ener){
	            this.consumoenergetico=conener;
	        }else{
	            this.consumoenergetico=error;}
	        }
	    
	    
	    private void colores(String color){
	    	 
	        String colores[]={"blanco", "negro", "rojo", "azul", "gris"};
	        boolean encon=false;
	 
	        for(int i=0;i<colores.length && !encon;i++){
	 
	            if(colores[i].equals(color)){
	                encon=true;
	            }
	 
	        }
	 
	        if(encon){
	            this.color=color;
	        }else{
	            this.color=error;}
	    }
	    
	    // Calcular plus en base a la letra del ele.
	    public double precioFinal(){
	        double plusprecio=0;
	        switch(consumoenergetico){
	            case "A":
	                plusprecio+=100;
	                break;
	            case "B":
	            	plusprecio+=80;
	                break;
	            case "C":
	            	plusprecio+=60;
	                break;
	            case "D":
	            	plusprecio+=50;
	                break;
	            case "E":
	            	plusprecio+=30;
	                break;
	            case "F":
	            	plusprecio+=10;
	                break;
	            default:
	            	System.out.println("No es una letra valida");
	        }
	        if(peso>=0 && peso<19){
	        	plusprecio+=10;
	        }else if(peso>=20 && peso<49){
	        	plusprecio+=50;
	        }else if(peso>=50 && peso<=79){
	        	plusprecio+=80;
	        }else if(peso>=80){
	        	plusprecio+=100;
	        }
	 
	        return preciobase+plusprecio;
	    }
		public int getPreciobase() {
			return preciobase;
		}
		public void setPreciobase(int preciobase) {
			this.preciobase = preciobase;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public String getConsumoenergetico() {
			return consumoenergetico;
		}
		public void setConsumoenergetico(String consumoenergetico) {
			this.consumoenergetico = consumoenergetico;
		}
		public int getPeso() {
			return peso;
		}
		public void setPeso(int peso) {
			this.peso = peso;
		}
		public int getPrecioFinal() {
			return precioFinal;
		}
		public void setPrecioFinal(int precioFinal) {
			this.precioFinal = precioFinal;
		}
		public String getError() {
			return error;
		}
		public void setError(String error) {
			this.error = error;
		}

}
