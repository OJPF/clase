/**
 * 
 */
package Extend;


/**
 * @author Oriol
 *
 */
public class main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Declaración variables
		
		double sumarelectrodomesticos = 0;
		double sumarlavadoras = 0;
		double sumartelevisores = 0;
		
		// Creando el array de 10 filas
		Electrodomestico listelectro[]=new Electrodomestico[10];
		
		// Asignar valores al array
		listelectro[0]=new Electrodomestico();
		listelectro[1]=new Lavadora();
		listelectro[2]=new Television();
		listelectro[3]=new Electrodomestico(50,5);
		listelectro[4]=new Lavadora();
		listelectro[5]=new Television(20,2);
		listelectro[6]=new Electrodomestico(5,"rojo","C",50);
		listelectro[7]=new Lavadora(45,"azul","F",20,2);
		listelectro[8]=new Television(60,"gris","D",15,50,true);
		listelectro[9]=new Electrodomestico(10,10);
		
		// Recorrer el array
		for(int i=0;i<listelectro.length;i++){
			if(listelectro[i]instanceof Electrodomestico){
			// Mostramos en la lista que sea verdad que busca electrodomestico.
				sumarelectrodomesticos+=listelectro[i].precioFinal();
				//Mostramos los resultados.
				System.out.println("Electrodomesticos"+sumarelectrodomesticos);
			}
				if(listelectro[i]instanceof Lavadora){
					sumarlavadoras+=listelectro[i].precioFinal();
					System.out.println("Lavadoras"+sumarlavadoras);
	            }
				if(listelectro[i]instanceof Television){
					sumartelevisores+=listelectro[i].precioFinal();
					System.out.println("Televisiones"+sumartelevisores);
	            }
				
            }
		}
	}

