/**
 * 
 */
package Arrays;

/**
 * @author Oriol
 *
 */
public class Eje4 {
	public static void main(String[] args){
		int operador1=3;
		int operador2=5;
		int resultado=sumaNumeros(operador1, operador2);
		System.out.println("El resultado de la suma es "+resultado);
	}
	public static int sumaNumeros(int num1,int num2){
		int resultado=num1+num2;
		return resultado;
	}
}
